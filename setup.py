#!/usr/bin/env python
#
# gdisper - GUI for the "disper" utility for NVidia cards
# http://thp.io/2011/gdisper/
#
#    Copyright (c) 2009 Thomas Jost
#    Copyright (c) 2011 Thomas Perl
#
#    gdisper is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published
#    by the Free Software Foundation, either version 3 of the License,
#    or (at your option) any later version.
#
#    gdisper is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with gdisper. If not, see <http://www.gnu.org/licenses/>.
#

from distutils.core import setup
import re

NAME = 'gdisper'
VERSION = '0.2'
AUTHOR = 'Thomas Jost <thomas.jost@gmail.com>'
DESCRIPTION = 'GUI for disper, a display switcher for nVidia cards.'
HOMEPAGE = 'http://thp.io/2011/gdisper/'

CLASSIFIERS = """
Development Status :: 4 - Beta
Environment :: X11 Applications :: GTK
Intended Audience :: End Users/Desktop
License :: OSI Approved :: GNU General Public License (GPL)
Natural Language :: English
Operating System :: POSIX :: Linux
Programming Language :: Python
Topic :: Desktop Environment
""".strip().splitlines()

SCRIPTS = [NAME]

DATA_FILES = [
    ('share/applications', ['gdisper.desktop']),
]

AUTHOR_NAME, EMAIL = re.match(r'^(.*) <(.*)>$', AUTHOR).groups()

setup(name=NAME,
      version=VERSION,
      description=DESCRIPTION,
      author=AUTHOR_NAME,
      author_email=EMAIL,
      classifiers=CLASSIFIERS,
      scripts=SCRIPTS,
      data_files=DATA_FILES,
      url=HOMEPAGE
)

